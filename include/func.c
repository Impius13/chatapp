#include <stdio.h>
#include <string.h>
#include "func.h"

void str_replace_newline(char* str) {
    str[strcspn(str, "\n")] = 0;
}

void str_overwrite_out() {
    printf("%s", "~ ");
    fflush(stdout);
}