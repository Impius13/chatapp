#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>

#include "communicationFunc.h"

void send_file(FILE* file, int sockfd) {
    char data[LENGTH];

    while (fgets(data, LENGTH, file) != NULL) {
        if (send(sockfd, data, sizeof(data), 0) == -1) {
            perror("\nError while sending file.");
            exit(1);
        }
        bzero(data, LENGTH);
    }
}

void receive_file(int sockfd) {
    int receive;
    FILE *file;
    char *filename = "received.txt";
    char buffer[LENGTH];

    file = fopen(filename, "w");
    while (1) {
        receive = recv(sockfd, buffer, LENGTH, 0);
        if (receive <= 0){
            break;
        }
        fprintf(file, "%s", buffer);
        bzero(buffer, LENGTH);
    }
}

