//
// Created by dadof on 28. 12. 2021.
//

#include "uthash.h"

#ifndef CHATAPP_USERSYSTEM_H
#define CHATAPP_USERSYSTEM_H

#define MAX_NAME_LEN 20
#define MAX_PASSWORD_LEN 25

// Structure for hash table
struct userStruct {
    char name[MAX_NAME_LEN];
    char password[MAX_PASSWORD_LEN];
    char friendList[255][MAX_NAME_LEN];
    unsigned int friendCount;

    UT_hash_handle hh;
};

int friendInList(char*, struct userStruct*);
int addFriend(char*, struct userStruct*);
int removeFriend(char*, struct userStruct*);
void loadData();
void saveData();
struct userStruct* login();
void registerUser();
void removeAccount(char*);

#endif //CHATAPP_USERSYSTEM_H
