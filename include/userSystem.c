#include <stdlib.h>
#include <stdio.h>
#include "uthash.h"
#include "func.h"

#include "userSystem.h"

#define FILE_NAME "user_data.txt"
#define MAX_NAME_LEN 20
#define MAX_PASSWORD_LEN 25

// Users hash table
struct userStruct* usersHash = NULL;

// Hash functions
struct userStruct* findUser(char* name) {
    struct userStruct* s = NULL;

    HASH_FIND_STR(usersHash, name, s);
    return s;
}

void addUser(char* inName, char* password) {
    struct userStruct* s = NULL;
    struct userStruct* duplicate;

    str_replace_newline(inName);
    str_replace_newline(password);

    // Add user if current name is not already used
    duplicate = findUser(inName);
    if (!duplicate) {
        s = malloc(sizeof(struct userStruct));
        strcpy(s->name, inName);
        strcpy(s->password, password);
        s->friendCount = 0;

        HASH_ADD_STR(usersHash, name, s);
    }
}

void deleteUser(char* name) {
    struct userStruct* s = findUser(name);
    if (s) {
        HASH_DEL(usersHash, s);
        free(s);
    }
}

int friendInList(char* friendName, struct userStruct* user) {
    // Check if user have friend on list
    int i;

    str_replace_newline(friendName);

    for (i = 0; i < user->friendCount; i++) {
        if (!strcmp(user->friendList[i], friendName)) {
            return 1;
        }
    }
    return 0;
}

int addFriend(char* friendName, struct userStruct* user) {
    struct userStruct* friend;

    str_replace_newline(friendName);

    friend = findUser(friendName);
    if (friend) {
        if (friendInList(friendName, user) == 0) {
            user->friendCount++;
            strcpy(user->friendList[user->friendCount - 1], friendName);

            return 1;
        }
    }
    return 0;
}

int removeFriend(char* friendName, struct userStruct* user) {
    int i;

    str_replace_newline(friendName);

    if (friendInList(friendName, user) == 1) {
        for (i = 0; i < user->friendCount; i++) {
            if (!strcmp(user->friendList[i], friendName)) {
                int x;
                for (x = user->friendCount - 1; x > i; x--) {
                    strcpy(user->friendList[x - 1], user->friendList[x]);
                }
                user->friendCount--;

                return 1;
            }
        }
        return 0;
    }
}

void loadUserFriends(struct userStruct* user) {
    // Load friend list from text file
    char fileName[255];
    char line[255];
    char friendName[MAX_NAME_LEN];

    sprintf(fileName, "%s_friends.txt", user->name);
    FILE* file;
    file = fopen(fileName, "r");

    if (file) {
        while (fgets(line, sizeof(line), file)) {
            strcpy(friendName, line);
            addFriend(friendName, user);
        }
        fclose(file);
    }
}

// File I/O
void loadData() {
    char name[MAX_NAME_LEN];
    char password[MAX_PASSWORD_LEN];
    char line[255];
    int iteration = 1;
    FILE* file;
    file = fopen(FILE_NAME, "r");

    if (file) {
        while (fgets(line, sizeof(line), file)) {
            if (iteration % 2 != 0) {
                strcpy(name, line);
            } else {
                strcpy(password, line);
                addUser(name, password);
            }
            iteration++;
        }
        fclose(file);

        // Add friends of users
        struct userStruct *s, *tmp;
        HASH_ITER(hh, usersHash, s, tmp) {
            loadUserFriends(s);
        }
    }
}

void saveData() {
    FILE *userDataFile, *userFriendList;
    char friendListFileName[255];
    int i;

    userDataFile = fopen(FILE_NAME, "w+");

    // Save to file and delete hash map
    struct userStruct *s, *tmp;
    HASH_ITER(hh, usersHash, s, tmp) {
        fprintf(userDataFile, "%s\n", s->name);
        fprintf(userDataFile, "%s\n", s->password);

        sprintf(friendListFileName, "%s_friends.txt", s->name);
        userFriendList = fopen(friendListFileName, "w");
        for (i = 0; i < s->friendCount; i++) {
            fprintf(userFriendList, "%s\n", s->friendList[i]);
        }
        fclose(userFriendList);
        bzero(friendListFileName, 255);

        HASH_DEL(usersHash, s);
        free(s);
    }
    fclose(userDataFile);
}


// Login and registration
struct userStruct* login() {
    char name[MAX_NAME_LEN], password[MAX_PASSWORD_LEN];
    struct userStruct* user;

    printf("\n***** LOGIN *****\n");

    // Name input
    while (1) {
        printf("\nEnter your login nickname: ");
        scanf("%s", &name);
        str_replace_newline(name);

        user = findUser(name);
        if (user) {
            break;
        } else {
            printf("\nUser with this name is not registered.");
        }
    }

    // Password input
    while (1) {
        printf("\nEnter password: ");
        scanf("%s", &password);
        str_replace_newline(password);

        if (strcmp(password, user->password) == 0) {
            break;
        } else {
            printf("\nWrong password!");
        }
    }

    printf("\nLogin successful!");
    return user;
}

void registerUser() {
    char name[MAX_NAME_LEN], password[MAX_PASSWORD_LEN], passwordCheck[MAX_PASSWORD_LEN];

    printf("\n***** REGISTRATION *****\n");

    // Name input
    while (1) {
        printf("\nEnter your login nickname: ");
        scanf("%s", &name);
        str_replace_newline(name);

        if (strlen(name) <= MAX_NAME_LEN && strlen(name) > 2) {
            if (!findUser(name)) {
                break;
            }

            printf("\nUser with this name already exists!");
        } else {
            printf("\nName must be at least 3 characters long and less than %d characters", MAX_NAME_LEN);
        }
    }

    // Password input
    while (1) {
        printf("\nEnter password: ");
        scanf("%s", &password);
        str_replace_newline(password);

        if (strlen(password) <= MAX_PASSWORD_LEN && strlen(password) > 7) {
            printf("\nRepeat password: ");
            scanf("%s", &passwordCheck);
            str_replace_newline(passwordCheck);

            if (strcmp(password, passwordCheck) == 0)
                break;

            printf("\nPasswords do not match!");
        } else {
            printf("\nPassword must be at least 8 characters long and less than %d characters", MAX_PASSWORD_LEN);
        }
    }

    // Save new user
    addUser(name, password);
    printf("\n\nYour registration was successful! \nWelcome %s!\n", name);

    saveData();
    loadData();
}

void removeAccount(char* name) {
    deleteUser(name);
    saveData();
    loadData();
}
