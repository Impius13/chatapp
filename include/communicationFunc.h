#include <stdlib.h>

#ifndef CHATAPP_COMMUNICATIONFUNC_H
#define CHATAPP_COMMUNICATIONFUNC_H

#define LENGTH 1024

void send_file(FILE*, int);
void receive_file(int);

#endif //CHATAPP_COMMUNICATIONFUNC_H
