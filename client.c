#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>

#include "include/func.h"
#include "include/userSystem.h"

#define LENGTH 2048

// Global variables
volatile sig_atomic_t flag = 0;
int sockfd = 0;
struct userStruct* currentUser = NULL;
int joinedRoom = 0;

pthread_mutex_t sendMutex;
pthread_cond_t sendCond;

void catch_ctrl_c_and_exit(int sig) {
    flag = 1;
}

void sendMessage(char* message) {
    send(sockfd, message, strlen(message), 0);
}

void send_msg_handler() {
    char message[LENGTH] = {};
    char buffer[LENGTH + MAX_NAME_LEN] = {};

    while(1) {
        str_overwrite_out();
        fgets(message, LENGTH, stdin);
        str_replace_newline(message);

        if (strcmp(message, "exit") == 0) {
            flag = 1;
            break;
        } else {
            sprintf(buffer, "%s: %s\n", currentUser->name, message);
            sendMessage(buffer);
        }

        if (flag)
            break;

        bzero(message, LENGTH);
        bzero(buffer, LENGTH + MAX_NAME_LEN);
    }
    catch_ctrl_c_and_exit(2);
}

void recv_msg_handler() {
    char message[LENGTH] = {};

    // Wait for client to join a room
    pthread_mutex_lock(&sendMutex);
    while (joinedRoom == 0) {
        pthread_cond_wait(&sendCond, &sendMutex);
    }
    pthread_mutex_unlock(&sendMutex);

    while (1) {
        int receive = recv(sockfd, message, LENGTH, 0);

        if (receive > 0) {
            printf("%s", message);
            str_overwrite_out();
        } else if (receive == 0) {
            break;
        } else {
            // Nothing
        }

        if (flag)
            break;

        memset(message, 0, sizeof(message));
    }
}

void friendListMenu() {
    char charChoice[3];
    int menuChoice = 0;
    char friendName[MAX_NAME_LEN];
    char message[LENGTH];
    char serverMessage[LENGTH];
    char clientMessage[LENGTH];
    unsigned int exit = 0;
    int i, receive;

    while (exit == 0) {
        printf("\n----- FRIEND LIST -----\n");
        printf("Your friends:\n");
        for (i = 0; i < currentUser->friendCount; i++) {
            printf("%s\n", currentUser->friendList[i]);
        }

        printf("\n1 - add friend\n");
        printf("2 - remove friend\n");
        printf("3 - send message to friend\n");
        printf("4 - exit\n");
        printf("> ");

        scanf("%s", charChoice);
        menuChoice = atoi(charChoice);

        switch (menuChoice) {
            case 1:
                printf("\nEnter name of the user: ");
                scanf("%s", &friendName);
                str_replace_newline(friendName);
                sprintf(clientMessage, "addfr_%s", friendName);
                sendMessage(clientMessage);

                while (receive <= 0) {
                    receive = recv(sockfd, serverMessage, LENGTH, 0);
                }

                if (!strcmp(serverMessage, "success")) {
                    if (addFriend(friendName, currentUser) == 1)
                        printf("\nSuccessfully added new friend!\n");
                    else
                        printf("\nUser with this name does not exist or you already have him on friend list.\n");
                }
                else {
                    printf("\nUser did not accept invitation or there was a communication problem.\n");
                }
                break;

            case 2:
                printf("\nEnter name of the user: ");
                scanf("%s", &friendName);
                str_replace_newline(friendName);
                sprintf(clientMessage, "removefr_%s", friendName);
                sendMessage(clientMessage);

                while (receive <= 0) {
                    receive = recv(sockfd, serverMessage, LENGTH, 0);
                }

                if (!strcmp(serverMessage, "success")) {
                    if (removeFriend(friendName, currentUser) == 1)
                        printf("\nSuccessfully removed user from friend list.\n");
                    else
                        printf("\nYou don't have this user on friend list.\n");
                }
                else {
                    printf("\nThere was a communication problem.\n");
                }
                break;

            case 3:
                printf("\nEnter name of friend: ");
                scanf("%s", &friendName);
                str_replace_newline(friendName);
                if (friendInList(friendName, currentUser) == 1) {
                    printf("\nWrite your message: \n> ");
                    scanf("%s", &message);
                    str_replace_newline(message);
                    sprintf(clientMessage, "sendmsg_%s_%s", friendName, message);
                    sendMessage(clientMessage);

                    while (receive <= 0) {
                        receive = recv(sockfd, serverMessage, LENGTH, 0);
                    }

                    if (!strcmp(serverMessage, "success")) {
                        printf("\nYour message was send successfully!\n");
                    } else {
                        printf("\nThere was a communication problem.\n");
                    }
                } else {
                    printf("\nYou don't have this user on friend list.\n");
                }

                break;

            case 4:
                exit = 1;
                break;

            default:
                printf("Invalid choice!\n");
                break;
        }

        //printf("\nLast server message in friend menu: %s\n", serverMessage);

        receive = 0;
    }

    // Save changes
    saveData();
    loadData();
}

void server_communication() {
    char mainClientMessage[LENGTH];
    char mainServerMessage[LENGTH];
    char roomList[LENGTH];
    char roomName[MAX_NAME_LEN];
    char charChoice[3];
    int menuChoice = 0;
    int receive = 0;

    unsigned int exit = 0;
    unsigned int startSending = 0;

    // Wait for room list
    while (1) {
        receive = recv(sockfd, roomList, LENGTH, 0);
        if (receive > 0)
            break;
    }
    receive = 0;

    pthread_mutex_lock(&sendMutex);
    while (exit == 0) {
        printf("\n\n----- SERVER MENU -----\n");
        printf("Logged as %s\n", currentUser->name);
        printf("Available rooms:\n%s\n", roomList);
        printf("1 - create room\n");
        printf("2 - join room\n");
        printf("3 - friends\n");
        printf("4 - disconnect\n");
        printf("> ");

        scanf("%s", charChoice);
        menuChoice = atoi(charChoice);

        switch (menuChoice) {
            case 1:
                printf("\nEnter new room name: ");
                scanf("%s", &roomName);
                str_replace_newline(roomName);
                sprintf(mainClientMessage, "create_%s", roomName);
                sendMessage(mainClientMessage);

                while (receive <= 0) {
                    receive = recv(sockfd, mainServerMessage, LENGTH, 0);
                }

                if (!strcmp(mainServerMessage, "success")) {
                    // Successfully created room
                    printf("\nSuccessfully created room %s.\n", roomName);
                    startSending = 1;
                } else if (!strcmp(mainServerMessage, "error")) {
                    printf("\nError: Room name is too long or room already exists.\n");
                } else if (receive > 0) {
                    printf("\n%s\n", mainServerMessage);
                }

                break;

            case 2:
                printf("\nEnter room name to join: ");
                scanf("%s", &roomName);
                str_replace_newline(roomName);
                sprintf(mainClientMessage, "join_%s", roomName);
                sendMessage(mainClientMessage);

                while (receive <= 0) {
                    receive = recv(sockfd, mainServerMessage, LENGTH, 0);
                }

                if (!strcmp(mainServerMessage, "success")) {
                    // Successfully joined room
                    printf("\nSuccessfully joined room %s.\n", roomName);
                    startSending = 1;
                } else if (!strcmp(mainServerMessage, "error")) {
                    printf("\nError: Room does not exist.\n");
                } else if (receive > 0) {
                    printf("\n%s\n", mainServerMessage);
                }

                break;

            case 3:
                friendListMenu();
                break;

            case 4:
                sendMessage("exit_1");
                exit = 1;
                flag = 1;
                joinedRoom = 1;
                pthread_mutex_unlock(&sendMutex);
                pthread_cond_signal(&sendCond);
                break;

            default:
                printf("Invalid choice!\n");
                break;
        }

        //printf("\nLast server message in server menu: %s\n", mainServerMessage);

        if (startSending == 1) {
            // Unlock mutex for receiving thread and move to sending messages
            printf("\nStarted sending messages\n");
            joinedRoom = 1;
            pthread_mutex_unlock(&sendMutex);
            pthread_cond_signal(&sendCond);
            send_msg_handler();
            exit = 1;
        }

        receive = 0;
    }
}

void loginMenu() {
    char charChoice[3];
    int menuChoice = 0;
    unsigned int exit = 0;

    while (exit == 0) {
        printf("\n----- LOGIN MENU -----\n");
        printf("1 - login\n");
        printf("2 - register\n");
        printf("3 - exit\n");
        printf("> ");

        scanf("%s", charChoice);
        menuChoice = atoi(charChoice);

        switch (menuChoice) {
            case 1:
                currentUser = login();
                exit = 1;
                break;

            case 2:
                registerUser();
                break;

            case 3:
                exit = 1;
                break;

            default:
                printf("Invalid choice!\n");
                break;
        }
    }
}

int main() {
    int port;
    char *ip = "127.0.0.1";;
    char charChoice[3];
    int mainMenuChoice = 0;
    unsigned int exit = 0;

    loadData();
    printf("/+++++ WELCOME TO THE CHATROOM +++++/\n");

    // Login
    loginMenu();

    if (!currentUser)
        // If user did not log in
        return 0;

    // Main menu
    while (exit == 0) {
        printf("\n\n----- CHAT APP -----\n");
        printf("Logged as %s\n", currentUser->name);
        printf("1 - connect to server\n");
        printf("2 - remove account\n");
        printf("3 - logout\n");
        printf("> ");

        scanf("%s", charChoice);
        mainMenuChoice = atoi(charChoice);

        switch (mainMenuChoice) {
            case 1:
                exit = 1;
                break;

            case 2:
                removeAccount(currentUser->name);
                currentUser = NULL;
                printf("\nAccount successfully removed.");

            case 3:
                currentUser = NULL;
                loginMenu();
                if (!currentUser)
                    return 0;
                break;

            default:
                printf("Invalid choice!\n");
                break;
        }
    }

    printf("\nEnter the number of port: ");
    scanf("%d", &port);

    signal(SIGINT, catch_ctrl_c_and_exit);

    struct sockaddr_in server_addr;

    // Socket settings
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(ip);
    server_addr.sin_port = htons(port);

    // Connect to Server
    int err = connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (err == -1) {
        printf("ERROR: Connection failed.\n");
        return EXIT_FAILURE;
    }

    // Send name
    send(sockfd, currentUser->name, MAX_NAME_LEN, 0);

    pthread_mutex_init(&sendMutex, NULL);
    pthread_cond_init(&sendCond, NULL);

    // Thread for server communication and sending messages
    pthread_t server_communication_thread;
    if (pthread_create(&server_communication_thread, NULL, (void *) server_communication, NULL) != 0) {
        printf("ERROR: pthread\n");
        return EXIT_FAILURE;
    }

    // Thread for receiving messages
    pthread_t recv_msg_thread;
    if (pthread_create(&recv_msg_thread, NULL, (void *) recv_msg_handler, NULL) != 0) {
        printf("ERROR: pthread\n");
        return EXIT_FAILURE;
    }

    while (1) {
        if (flag) {
            printf("\nBye\n");
            break;
        }
    }

    pthread_join(server_communication_thread, NULL);
    pthread_join(recv_msg_thread, NULL);

    pthread_mutex_destroy(&sendMutex);
    pthread_cond_destroy(&sendCond);
    close(sockfd);

    saveData();

    return EXIT_SUCCESS;
}