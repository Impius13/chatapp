#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <signal.h>


#include "include/func.h"
#include "include/userSystem.h"

#define MAX_CLIENTS 100
#define MAX_ROOMS 10
#define BUFFER_SIZE 2048

static _Atomic unsigned int cli_count = 0;
static int uid = 10;

// Client structure
typedef struct {
    struct sockaddr_in address;
    int sockfd;
    int uid;
    char name[MAX_NAME_LEN];
    char room[MAX_NAME_LEN];
} client_t;

client_t *clients[MAX_CLIENTS];

char rooms[MAX_ROOMS][MAX_NAME_LEN] = {""};
unsigned int roomCount = 0;

pthread_mutex_t clients_mutex = PTHREAD_MUTEX_INITIALIZER;

void print_client_addr(struct sockaddr_in addr){
    printf("%d.%d.%d.%d",
           addr.sin_addr.s_addr & 0xff,
           (addr.sin_addr.s_addr & 0xff00) >> 8,
           (addr.sin_addr.s_addr & 0xff0000) >> 16,
           (addr.sin_addr.s_addr & 0xff000000) >> 24);
}

int create_room(char *roomName) {
    // Check for name duplicity
    int i;
    for (i = 0; i < roomCount; i++) {
        if (!strcmp(rooms[i], roomName)) {
            return 0;
        }
    }

    if (strlen(roomName) > MAX_NAME_LEN)
        return 0;

    roomCount++;
    strcpy(rooms[roomCount - 1], roomName);
    return 1;
}

int join_room(char *roomName, client_t *cl) {
    int i;
    for (i = 0; i < roomCount; i++) {
        if (!strcmp(rooms[i], roomName)) {
            strcpy(cl->room, rooms[i]);
            return 1;
        }
    }

    return 0;
}

// Get client UID by name
int getClientUid(char* clientName) {
    int i;
    for (i = 0; i < MAX_CLIENTS; ++i) {
        if (clients[i]) {
            if (!strcmp(clients[i]->name, clientName)) {
                return clients[i]->uid;
            }
        }
    }

    return -1;
}

// Add client to queue
void queue_add(client_t *cl) {
    pthread_mutex_lock(&clients_mutex);

    int i;
    for (i = 0; i < MAX_CLIENTS; ++i){
        if (!clients[i]) {
            clients[i] = cl;
            break;
        }
    }

    pthread_mutex_unlock(&clients_mutex);
}

// Remove client from queue
void queue_remove(int uid) {
    pthread_mutex_lock(&clients_mutex);

    int i;
    for (i = 0; i < MAX_CLIENTS; ++i) {
        if (clients[i]) {
            if (clients[i]->uid == uid) {
                clients[i] = NULL;
                break;
            }
        }
    }

    pthread_mutex_unlock(&clients_mutex);
}

// Send message to one client
void send_message_client(char *s, int uid) {
    pthread_mutex_lock(&clients_mutex);

    int i;
    for (i = 0; i < MAX_CLIENTS; ++i){
        if (clients[i]) {
            if (clients[i]->uid == uid) {
                if (write(clients[i]->sockfd, s, strlen(s)) < 0) {
                    perror("ERROR: write to descriptor failed");
                    break;
                }
            }
        }
    }

    pthread_mutex_unlock(&clients_mutex);
}

// Send message to all clients in room except sender
void send_message(char *s, int uid, char *room) {
    pthread_mutex_lock(&clients_mutex);

    int i;
    for (i = 0; i < MAX_CLIENTS; ++i){
        if (clients[i]) {
            if (!strcmp(clients[i]->room, room) && clients[i]->uid != uid) {
                if (write(clients[i]->sockfd, s, strlen(s)) < 0) {
                    perror("ERROR: write to descriptor failed");
                    break;
                }
            }
        }
    }

    pthread_mutex_unlock(&clients_mutex);
}

// Thread function
// Handle all communication with the client
void *handle_client(void *arg) {
    char buff_out[BUFFER_SIZE];
    char commandMessage[BUFFER_SIZE];
    char *commandToken;
    char name[MAX_NAME_LEN];
    int leave_flag = 0;
    int exitLoop = 0;
    int clientReceive = 0;

    cli_count++;
    client_t *cli = (client_t *)arg;

    // Check name
    if (recv(cli->sockfd, name, MAX_NAME_LEN, 0) <= 0 || strlen(name) <  2 || strlen(name) >= MAX_NAME_LEN - 1) {
        printf("Didn't enter the name.\n");
        leave_flag = 1;
    } else {
        strcpy(cli->name, name);
    }

    bzero(buff_out, BUFFER_SIZE);

    // Send room list to client
    int i;
    char roomName[MAX_NAME_LEN];
    for (i = 0; i < roomCount; i++) {
        sprintf(roomName, "%s\n", rooms[i]);
        if (i == 0)
            sprintf(buff_out, "%s", roomName);
        else
            strcat(buff_out, roomName);
    }
    send_message_client(buff_out, cli->uid);

    bzero(buff_out, BUFFER_SIZE);

    // Main server menu loop
    while (exitLoop == 0) {
        // Wait for client command
        while (clientReceive <= 0)
            clientReceive = recv(cli->sockfd, buff_out, BUFFER_SIZE, 0);
        strcpy(commandMessage, buff_out);

        printf("\nClient command: %s\n", commandMessage);

        /* Server commands */
        // addfr_ - add friend
        // removefr_ - remove friend
        // sendmsg_ - send encrypted message to friend
        // sendfile_ - send file to friend
        // create_ - create room
        // join_ - join room
        // exit_ - disconnect server
        commandToken = strtok(commandMessage, "_");

        if (!strcmp(commandToken, "addfr")) {
            char *friendName = strtok(NULL, "_");
            /*int frUid = getClientUid(friendName);
            if (frUid >= 0) {
                send_message_client("success", cli->uid);
            }
            else {
                send_message_client("error", cli->uid);
            }*/
            send_message_client("success", cli->uid);
        }
        else if (!strcmp(commandToken, "removefr")) {
            char *friendName = strtok(NULL, "_");
            /*int frUid = getClientUid(friendName);
            if (frUid >= 0) {
                send_message_client("success", cli->uid);
            }
            else {
                send_message_client("error", cli->uid);
            }*/
            send_message_client("success", cli->uid);
        }
        else if (!strcmp(commandToken, "sendmsg")) {
            char *friendName = strtok(NULL, "_");
            int frUid = getClientUid(friendName);
            if (frUid >= 0) {
                char* message;
                sprintf(message, "%s : %s\n", cli->name, strtok(NULL, "_"));
                send_message_client(message, frUid);
                send_message_client("success", cli->uid);
            }
            else {
                send_message_client("error", cli->uid);
            }
        }
        else if (!strcmp(commandToken, "create")) {
            char *roomName = strtok(NULL, "_");
            if (create_room(roomName) == 1) {
                send_message_client("success", cli->uid);
                join_room(roomName, cli);
                exitLoop = 1;
            }
            else {
                send_message_client("error", cli->uid);
            }
        }
        else if (!strcmp(commandToken, "join")) {
            char *roomName = strtok(NULL, "_");
            if (join_room(roomName, cli) == 1) {
                send_message_client("success", cli->uid);
                exitLoop = 1;
            }
            else {
                send_message_client("error", cli->uid);
            }
        }
        else if (!strcmp(commandToken, "exit")) {
            leave_flag = 1;
            exitLoop = 1;
        }

        bzero(buff_out, BUFFER_SIZE);
        bzero(commandMessage, BUFFER_SIZE);
        clientReceive = 0;
    }

    bzero(buff_out, BUFFER_SIZE);

    // Inform about joined client
    if (leave_flag != 1) {
        sprintf(buff_out, "%s has joined in room %s\n", cli->name, cli->room);
        printf("%s", buff_out);
        send_message(buff_out, cli->uid, cli->room);
    }

    bzero(buff_out, BUFFER_SIZE);

    // Loop for room chatting
    while (1) {
        if (leave_flag) {
            break;
        }

        // Handle message
        int receive = recv(cli->sockfd, buff_out, BUFFER_SIZE, 0);

        if (receive == 0 || strcmp(buff_out, "exit") == 0) {
            // Client wants to exit
            sprintf(buff_out, "%s has left\n", cli->name);
            printf("%s", buff_out);
            send_message(buff_out, cli->uid, cli->room);
            send_message_client("", cli->uid);
            leave_flag = 1;
        }
        else if (receive > 0) {
            if (strlen(buff_out) > 0) {
                // Send message
                send_message(buff_out, cli->uid, cli->room);

                str_replace_newline(buff_out);
                printf("%s -> %s\n", buff_out, cli->room);
            }
        } else {
            printf("ERROR: -1\n");
            leave_flag = 1;
        }

        bzero(buff_out, BUFFER_SIZE);
    }

    // Delete client from queue and yield thread
    close(cli->sockfd);
    queue_remove(cli->uid);
    free(cli);
    cli_count--;
    pthread_detach(pthread_self());

    return NULL;
}


int main() {
    int port;
    printf("Enter the number of port: ");
    scanf("%d", &port);

    char *ip = "127.0.0.1";
    int option = 1;
    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr;
    struct sockaddr_in cli_addr;
    pthread_t tid;

    // Load user system
    loadData();

    // Socket settings
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);
    serv_addr.sin_port = htons(port);

    // Ignore pipe signals
    signal(SIGPIPE, SIG_IGN);

    if (setsockopt(listenfd, SOL_SOCKET,(SO_REUSEPORT | SO_REUSEADDR), (char*)&option, sizeof(option)) < 0) {
        perror("ERROR: setsockopt failed");
        return EXIT_FAILURE;
    }

    // Bind
    if (bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("ERROR: Socket binding failed");
        return EXIT_FAILURE;
    }

    // Listen
    if (listen(listenfd, 10) < 0) {
        perror("ERROR: Socket listening failed");
        return EXIT_FAILURE;
    }

    printf("/+++++ WELCOME TO THE CHATROOM SERVER +++++/\n");

    // Add general chat room
    create_room("general");

    while (1) {
        socklen_t cli_size = sizeof(cli_addr);
        connfd = accept(listenfd, (struct sockaddr*)&cli_addr, &cli_size);

        // Check if max clients is reached
        if ((cli_count + 1) == MAX_CLIENTS) {
            printf("Max clients reached. Rejected: ");
            print_client_addr(cli_addr);
            printf(":%d\n", cli_addr.sin_port);
            close(connfd);
            continue;
        }

        // Client settings
        client_t *cli = (client_t *)malloc(sizeof(client_t));
        cli->address = cli_addr;
        cli->sockfd = connfd;
        cli->uid = uid++;

        // Add client to the queue and fork thread
        queue_add(cli);
        pthread_create(&tid, NULL, &handle_client, (void*)cli);

        // Reduce CPU usage
        sleep(1);
    }
}